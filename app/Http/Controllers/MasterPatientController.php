<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class MasterPatientController extends Controller
{
	public function index(){
		$judul = "Master Patient";

	    $query = DB::table('master_patient')->select('master_patient.id', 'master_patient.nama', 'master_patient.jenis_kelamin', 'master_patient.umur', 'master_patient.alamat', 'master_room.room')
	    ->leftjoin('master_room', 'master_patient.no_rm', '=', 'master_room.id');
	    $data_room = $query->get();
	    
	    $query_tipe=DB::table('master_room')->get();
		$data['room']=$query_tipe;

		$data['title'] = $judul;
	    $data['patient'] = $data_room;

		return view('pages.masters.master_patient',$data);
	}

	public function input(Request $request){
	    $name = $request->input('nama');
	    $ruangan = $request->input('ruangan');
	    $jenis_kelamin = $request->input('jenis_kelamin');
	    $umur = $request->input('umur');
	    $alamat = $request->input('alamat');

	    DB::table('master_patient')
	    	->insert(['nama'=>$name,
				'no_rm'=>$ruangan,
				'jenis_kelamin'=>$jenis_kelamin,
				'umur'=>$umur,
				'alamat'=>$alamat,
		]);

		return redirect('master-patient');
	}

	//UPDATE
	public function update($id){
	    $judul = "Edit Master Patient";
	    $data['title'] = $judul;

	    $query = DB::table('master_patient')->where('id', $id)->first();
	    $data['patient']=$query;

	    $query_tipe=DB::table('master_room')->get();
	    $data['room']=$query_tipe;

	    return view('pages.masters.master_patient_update', $data);
	}

	public function updatePost(Request $request){
		$id = $request->input('id');
	    $name = $request->input('nama');
	    $ruangan = $request->input('ruangan');
	    $jenis_kelamin = $request->input('jenis_kelamin');
	    $umur = $request->input('umur');
	    $alamat = $request->input('alamat');

	    DB::table('master_patient')->where('id', $id)
        ->update(['nama'=>$name,
				'no_rm'=>$ruangan,
				'jenis_kelamin'=>$jenis_kelamin,
				'umur'=>$umur,
				'alamat'=>$alamat,
		]);

		return redirect('master-patient');
	}

	public function delete($id) {
	    $query = DB::table('master_patient')->where('id', $id);
	    $data_test = $query->first();
	    DB::delete('delete from master_patient where id = ?',[$id]);
	    
	    return redirect('master-patient');
	}
}
