<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Orders;

class MasterGroupTestController extends Controller
{
    public function index(){
    	$judul = "Master Group Test";

        $query = DB::table('master_groups');
        $data_group = $query->get();
        $data['groups'] = $data_group;

        $query = DB::table('master_tests');
        $data_test = $query->get();
        $data['tests'] = $data_test;

        $query = DB::table('master_group_tests');
        $data_group_test = $query->get();

    	$data['title'] = $judul;
        $data['group_tests'] = $data_group_test;

    	return view('pages.masters.master_group_test',$data);
    }

    public function input(Request $request){
        foreach ($request->input('tests') as $test) {

            $group = $request->input('group');

            DB::table('master_group_tests')
            ->insert(
                ['master_test_id'=>$test,
                'master_group_id'=>$group
                ]);
        }

    	return redirect('master_group_test');
    }

    //UPDATE
    public function update($id){
        $judul = "Edit Master Group";

        $query = DB::table('master_groups')->where('id', $id);
        $data_test = $query->first();

        $data['title'] = $judul;
        $data['groups'] = $data_test;
        return view('pages.masters.master_group_update',$data);
    }

    public function updatePost(Request $request){
        $id = $request->input('id');    
        $name = $request->input('name');

        DB::table('master_groups')
        ->where('id', $id)
        ->update(
            ['name'=>$name]);

        return redirect('master_group');
    }

    public function delete($id) {
        $query = DB::table('master_groups')->where('id', $id);
        $data_test = $query->first();
        DB::delete('delete from master_groups where id = ?',[$id]);
        
        return redirect('master_group');
    }

}
