<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class MasterInsuraceController extends Controller
{
		public function index(){
			$judul = "Master Insurace";

		    $query = DB::table('master_insurace');
		    $data_insurace = $query->get();

			$data['title'] = $judul;
		    $data['insurace'] = $data_insurace;

			return view('pages.masters.master_insurace',$data);
		}

		public function input(Request $request){
		    $insurace = $request->input('insurace');
		    $discount = $request->input('discount');

		    DB::table('master_insurace')
		    ->insert(['nama_insurace'=>$insurace,
		    	'discount'=>$discount,
			]);

			return redirect('master-insurace');
		}

		public function update($id)
		{
		    $judul = "Edit Master Insurace";

		    $query = DB::table('master_insurace')->where('id', $id);
		    $data_test = $query->first();

		    $data['title'] = $judul;
		    $data['insurace'] = $data_test;

		    return view('pages.masters.master_insurace_update', $data);
		}

		public function updatePost(Request $request){
		    $id = $request->input('id');    
		    $insurace = $request->input('insurace');
		    $discount = $request->input('discount');
		    

		    DB::table('master_insurace')->where('id', $id)
		    ->update(['nama_insurace' => $insurace,
		    		'discount'=>$discount,
		        ]);

			return redirect('master-insurace');
		}

		public function delete($id) {
	        $query = DB::table('master_insurace')->where('id', $id);
	        $data_test = $query->first();
	        DB::delete('delete from master_insurace where id = ?',[$id]);
	        
	        return redirect('master-insurace');
	    }
}
