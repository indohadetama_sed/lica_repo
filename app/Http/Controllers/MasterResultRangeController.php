<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MasterResultRangeController extends Controller
{
    public function index(){
    	$judul = "Master Result Range";

        $query = DB::table('master_result_ranges');
        $data_result_range = $query->get();

    	$data['title'] = $judul;
        $data['result_ranges'] = $data_result_range;

    	return view('pages.masters.master_result_range',$data);
    }

    public function input(Request $request){
        $min_age = $request->input('age-below');
        $max_age = $request->input('age-above');
        $male_ref = $request->input('male-ref');
        $female_ref = $request->input('female-ref');
        $male_crit = $request->input('male-crit');
        $female_crit = $request->input('female-crit');

        DB::table('master_result_ranges')
        ->insert(
            ['min_age'=>$min_age,
            'max_age'=>$max_age,
            'male_ref'=>$male_ref,
            'female_ref'=>$female_ref,
            'male_crit'=>$male_crit,
            'female_crit' =>$female_crit,
        ]);

    	return redirect('master-result-range');
    }

    //UPDATE
    public function update($id){
        $judul = "Edit Master Result Range";

        $query = DB::table('master_result_ranges')->where('id', $id);
        $data_result_range = $query->first();

        $data['title'] = $judul;
        $data['result_ranges'] = $data_result_range;
        return view('pages.masters.master_result_range_update',$data);
    }

    public function updatePost(Request $request){
        $id = $request->input('id');    
        $min_age = $request->input('age-below');
        $max_age = $request->input('age-above');
        $male_ref = $request->input('male-ref');
        $female_ref = $request->input('female-ref');
        $male_crit = $request->input('male-crit');
        $female_crit = $request->input('female-crit');

        DB::table('master_result_ranges')
        ->where('id', $id)
        ->update(
            ['min_age'=>$min_age,
            'max_age'=>$max_age,
            'male_ref'=>$male_ref,
            'female_ref'=>$female_ref,
            'male_crit'=>$male_crit,
            'female_crit' =>$female_crit,
        ]);

        return redirect('master-result-range');
    }

    public function delete($id) {
        $query = DB::table('master_result_ranges')->where('id', $id);
        $data_test = $query->first();
        DB::delete('delete from master_result_ranges where id = ?',[$id]);
        
        return redirect('master-result-range');
    }

}
