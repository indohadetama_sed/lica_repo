<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class MasterDokterController extends Controller
{
		public function index(){
			$judul = "Master Dokter";

		    $query = DB::table('master_dokter');
		    $data_dokter = $query->get();

			$data['title'] = $judul;
		    $data['dokter'] = $data_dokter;

			return view('pages.masters.master_dokter',$data);
		}

		public function input(Request $request){
		    $nama = $request->input('name');
		    $title_dokter = $request->input('title_doctor');

		    DB::table('master_dokter')
		    ->insert(['name'=>$nama,
		    	'title_doctor'=>$title_dokter,
			]);

			return redirect('master-dokter');
		}

		public function update($id)
		{
		    $judul = "Edit Master Dokter";

		    $query = DB::table('master_dokter')->where('id', $id);
		    $data_test = $query->first();

		    $data['title'] = $judul;
		    $data['dokter'] = $data_test;

		    return view('pages.masters.master_dokter_update', $data);
		}

		public function updatePost(Request $request){
		    $id = $request->input('id');    
		    $nama = $request->input('nama');
		    $title_dokter = $request->input('title_dokter');
		    

		    DB::table('master_dokter')->where('id', $id)
		    ->update(['name' => $nama,
		    		'title_doctor'=>$title_dokter,
		        ]);

			return redirect('master-dokter');
		}

		public function delete($id) {
	        $query = DB::table('master_dokter')->where('id', $id);
	        $data_test = $query->first();
	        DB::delete('delete from master_dokter where id = ?',[$id]);
	        
	        return redirect('master-dokter');
	    }
}
