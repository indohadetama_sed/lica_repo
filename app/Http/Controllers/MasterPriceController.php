<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class MasterPriceController extends Controller
{
    public function index()
    {
    	$judul = "Master Prices";

        $prices = DB::table('master_prices')->select('master_prices.id', 'master_tests.name', 'master_prices.price')->leftjoin('master_tests', 'master_prices.master_test_id', '=', 'master_tests.id');
        $data_test = $prices->get();

        $query_tipe=DB::table('master_tests')->get();
        $data['master_tes']=$query_tipe;

    	$data['title'] = $judul;
        $data['prices'] = $data_test;

    	return view('pages.masters.master_price',$data);
    }

    public function input(request $request)
    {
        $name = $request->input('name');
        $master_test = $request->input('master_tes');

        DB::table('master_prices')
        ->insert(['price'=>$name,
        	 'master_test_id'=>$master_test,
            ]);

    	return redirect()->action('MasterPriceController@index');
    }

    public function update($id)
    {
        $judul = "Edit Master Prices";
        $data['title'] = $judul;

        $query = DB::table('master_prices')->where('id', $id)->first();
        $data['master_pri']=$query;

        $query_tipe=DB::table('master_tests')->get();
        $data['master_tes']=$query_tipe;

        return view('pages.masters.master_price_update', $data);
    }

    public function updatePost(Request $request){
        $id = $request->input('id');    
        $nama = $request->input('nama');
        $master_test = $request->input('master_test');

        DB::table('master_prices')->where('id', $id)
        ->update(
            ['price' => $nama,
             'master_test_id' => $master_test,
            ]);

    	return redirect()->action('MasterPriceController@index');
    }

    public function delete($id) {
        $query = DB::table('master_analyzers')->where('id', $id);
        $data_test = $query->first();
        DB::delete('delete from master_prices where id = ?',[$id]);
        
        return redirect()->action('MasterPriceController@index');
    }
}
