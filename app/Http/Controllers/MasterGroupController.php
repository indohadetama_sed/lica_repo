<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MasterGroupController extends Controller
{
    public function index(){
    	$judul = "Master Group";

        $query = DB::table('master_groups');
        $data_group = $query->get();

    	$data['title'] = $judul;
        $data['groups'] = $data_group;

    	return view('pages.masters.master_group',$data);
    }

    public function input(Request $request){
        $name = $request->input('name');

        DB::table('master_groups')
        ->insert(
            ['name'=>$name]);

    	return redirect('master-group');
    }

    //UPDATE
    public function update($id){
        $judul = "Edit Master Group";

        $query = DB::table('master_groups')->where('id', $id);
        $data_test = $query->first();

        $data['title'] = $judul;
        $data['groups'] = $data_test;
        return view('pages.masters.master_group_update',$data);
    }

    public function updatePost(Request $request){
        $id = $request->input('id');    
        $name = $request->input('name');

        DB::table('master_groups')
        ->where('id', $id)
        ->update(
            ['name'=>$name]);

        return redirect('master-group');
    }

    public function delete($id) {
        $query = DB::table('master_groups')->where('id', $id);
        $data_test = $query->first();
        DB::delete('delete from master_groups where id = ?',[$id]);
        
        return redirect('master-group');
    }

}
