<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class MasterSpecimenController extends Controller
{
    public function index()
    {
    	$judul = "Master Specimens";

        $specimens = DB::table('master_specimens');
        $data_test = $specimens->get();

    	$data['title'] = $judul;
        $data['specimens'] = $data_test;

    	return view('pages.masters.master_specimen',$data);
    }

    public function input(request $request)
    {
        $name = $request->input('name');

        DB::table('master_specimens')
        ->insert(
        	['specimens'=>$name,
            ]);

    	return redirect()->action('MasterSpecimenController@index');
    }

    public function update($id)
    {
        $judul = "Edit Master Specimens";

        $query = DB::table('master_specimens')->where('id', $id);
        $data_test = $query->first();

        $data['title'] = $judul;
        $data['specimens'] = $data_test;

        return view('pages.masters.master_specimen_update', $data);
    }

    public function updatePost(Request $request){
        $id = $request->input('id');    
        $nama = $request->input('nama');
        

        DB::table('master_specimens')->where('id', $id)
        ->update(
            ['specimens' => $nama,
            ]);

    	return redirect()->action('MasterSpecimenController@index');
    }

    public function delete($id) {
        $query = DB::table('master_specimens')->where('id', $id);
        $data_test = $query->first();
        DB::delete('delete from master_specimens where id = ?',[$id]);
        
        return redirect()->action('MasterSpecimenController@index');
    }
}
