<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MasterResultController extends Controller
{
    public function index(){
    	$judul = "Master Result";

        $query = DB::table('master_results');
        $data_result = $query->get();

    	$data['title'] = $judul;
        $data['results'] = $data_result;

    	return view('pages.masters.master_result',$data);
    }

    public function input(Request $request){
        $result = $request->input('result');

        DB::table('master_results')
        ->insert(
            ['result'=>$result]);

    	return redirect('master-result');
    }

    //UPDATE
    public function update($id){
        $judul = "Edit Master Result";

        $query = DB::table('master_results')->where('id', $id);
        $data_result = $query->first();

        $data['title'] = $judul;
        $data['results'] = $data_result;
        return view('pages.masters.master_result_update',$data);
    }

    public function updatePost(Request $request){
        $id = $request->input('id');    
        $result = $request->input('result');

        DB::table('master_results')
        ->where('id', $id)
        ->update(
            ['result'=>$result]);

        return redirect('master-result');
    }

    public function delete($id) {
        $query = DB::table('master_results')->where('id', $id);
        $data_result = $query->first();
        DB::delete('delete from master_results where id = ?',[$id]);
        
        return redirect('master-result');
    }

}
