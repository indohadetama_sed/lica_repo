<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MasterTestController extends Controller
{
    public function index(){
    	$judul = "Master Test";

        $query = DB::table('master_tests');
        $data_test = $query->get();

    	$data['title'] = $judul;
        $data['tests'] = $data_test;

    	return view('pages.masters.master_test',$data);
    }


    // public function input(){
    //     $query = DB::table('master_tests');
    //     $data_test = $query->get();

    //     $data['tests'] = $data_test;
    // 	return view('latihan.input',$data);
    // }

    public function input(Request $request){    
        $name = $request->input('name');
        $initial = $request->input('initial');
        $unit = $request->input('unit');
        $analyzer = $request->input('analyzer');
        $specimen = $request->input('specimen');

        DB::table('master_tests')
        ->insert(
            ['name'=>$name,
            'initial'=>$initial,
            'unit'=>$unit,
            'master_analyzer_id'=>$analyzer,
            'master_specimen_id'=>$specimen
            ]);

    	return redirect('master_test');
    }

    //UPDATE
    public function update($id){
        $judul = "Edit Master";

        $query = DB::table('master_tests')->where('id', $id);
        $data_test = $query->first();

        $data['title'] = $judul;
        $data['tests'] = $data_test;
        return view('pages.masters.master_test_update',$data);
    }

    public function updatePost(Request $request){
        $id = $request->input('id');    
        $name = $request->input('name');
        $initial = $request->input('initial');
        $unit = $request->input('unit');
        $analyzer = $request->input('analyzer');
        $specimen = $request->input('specimen');

        DB::table('master_tests')
        ->where('id', $id)
        ->update(
            ['name'=>$name,
            'initial'=>$initial,
            'unit'=>$unit,
            'master_analyzer_id'=>$analyzer,
            'master_specimen_id'=>$specimen
            ]);

        return redirect('master_test');
    }

    public function delete($id) {
        $query = DB::table('master_tests')->where('id', $id);
        $data_test = $query->first();
        DB::delete('delete from master_tests where id = ?',[$id]);

        $query = DB::table('master_group_tests')->where('master_test_id', $id);
        $data_test = $query->first();
        DB::delete('delete from master_group_tests where master_test_id = ?',[$id]);
        
        return redirect('master_test');
    }

}
