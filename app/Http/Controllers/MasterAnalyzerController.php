<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class MasterAnalyzerController extends Controller
{
    public function index()
    {
    	$judul = "Master Analyzers";

        $analyzers = DB::table('master_analyzers');
        $data_test = $analyzers->get();

    	$data['title'] = $judul;
        $data['analyzers'] = $data_test;

    	return view('pages.masters.master_analyzer',$data);
    }

    public function input(request $request)
    {
        $name = $request->input('name');

        DB::table('master_analyzers')
        ->insert(
        	['analyzer'=>$name,
            ]);

    	return redirect()->action('MasterAnalyzerController@index');
    }

    public function update($id)
    {
        $judul = "Edit Master Analyzers";

        $query = DB::table('master_analyzers')->where('id', $id);
        $data_test = $query->first();

        $data['title'] = $judul;
        $data['analyzer'] = $data_test;

        return view('pages.masters.master_analyzer_update', $data);
    }

    public function updatePost(Request $request){
        $id = $request->input('id');    
        $nama = $request->input('nama');
        

        DB::table('master_analyzers')->where('id', $id)
        ->update(
            ['analyzer' => $nama,
            ]);

    	return redirect()->action('MasterAnalyzerController@index');
    }

    public function delete($id) {
        $query = DB::table('master_analyzers')->where('id', $id);
        $data_test = $query->first();
        DB::delete('delete from master_analyzers where id = ?',[$id]);
        
        return redirect()->action('MasterAnalyzerController@index');
    }
}
