<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class MasterRoomController extends Controller
{
	public function index(){
		$judul = "Master Room";

	    $query = DB::table('master_room');
	    $data_room = $query->get();

		$data['title'] = $judul;
	    $data['room'] = $data_room;

		return view('pages.masters.master_room',$data);
	}

	public function input(Request $request){
	    $ruangan = $request->input('ruangan');
	    $kelas = $request->input('kelas');

	    DB::table('master_room')
	    ->insert(['room'=>$ruangan,
	    	'kelas'=>$kelas,
		]);

		return redirect('master-room');
	}

	public function update($id)
	{
	    $judul = "Edit Master Room";

	    $query = DB::table('master_room')->where('id', $id);
	    $data_test = $query->first();

	    $data['title'] = $judul;
	    $data['room'] = $data_test;

	    return view('pages.masters.master_room_update', $data);
	}

	public function updatePost(Request $request){
	    $id = $request->input('id');    
	    $ruangan = $request->input('ruangan');
	    $kelas = $request->input('kelas');
	    

	    DB::table('master_room')->where('id', $id)
	    ->update(['room' => $ruangan,
	    		'kelas'=>$kelas,
	        ]);

		return redirect('master-room');
	}

	public function delete($id) {
        $query = DB::table('master_room')->where('id', $id);
        $data_test = $query->first();
        DB::delete('delete from master_room where id = ?',[$id]);
        
        return redirect()->action('MasterRoomController@index');
    }
}
