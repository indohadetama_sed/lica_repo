<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class MasterFormulaController extends Controller
{
		public function index(){
			$judul = "Master Formula";

		   	$formula = DB::table('master_formula')->select('master_formula.id','master_formula.id_test','master_analyzers.analyzer','master_prices.price', 'master_specimens.specimens', 'master_formula.formula')
		   	->leftjoin('master_prices','master_formula.prices', '=', 'master_prices.id')
		   	->leftjoin('master_analyzers','master_formula.analyzer', '=', 'master_analyzers.id')
		   	->leftjoin('master_specimens','master_formula.specimens', '=', 'master_specimens.id');
		   	$data_formula=$formula->get();

		    $price=DB::table('master_prices')->get();
			$data['price']=$price;

			$analyzer=DB::table('master_analyzers')->get();
			$data['analyzer']=$analyzer;

			$specimens=DB::table('master_specimens')->get();
			$data['specimen']=$specimens;

		   	$data['formula'] = $data_formula;
			$data['title'] = $judul;

			return view('pages.masters.master_formula',$data);
		}

		public function input(Request $request){
		    $id_test = $request->input('id-test');
		    $price = $request->input('master-prices');
		    $analyzer = $request->input('master-analyzer');
		    $specimen = $request->input('master-specimen');
		    $formula = $request->input('formula');

		    DB::table('master_formula')
		    	->insert(['id_test'=>$id_test,
					'prices'=>$price,
					'analyzer'=>$analyzer,
					'specimens'=>$specimen,
					'formula'=>$formula,
			]);

			return redirect('master-formula');
		}

		//UPDATE
		public function update($id){
		    $judul = "Edit Master Formula";
		    $data['title'] = $judul;

		    $query = DB::table('master_formula')->where('id', $id)->first();
		    $data['formula']=$query;

		    $query_tipe=DB::table('master_specimens')->get();
		    $data['specimen']=$query_tipe;

		    $query_tipe=DB::table('master_analyzers')->get();
		    $data['analyzer']=$query_tipe;

		    $query_tipe=DB::table('master_prices')->get();
		    $data['price']=$query_tipe;

		    return view('pages.masters.master_formula_update', $data);
		}

		public function updatePost(Request $request){
			$id = $request->input('id');
		    $id_test = $request->input('id_test');
		    $price = $request->input('prices');
		    $analyzer = $request->input('analyzer');
		    $specimen = $request->input('specimen');
		    $formula = $request->input('formula');

		    DB::table('master_formula')->where('id', $id)
	        ->update(['id_test'=>$id_test,
					'prices'=>$price,
					'analyzer'=>$analyzer,
					'specimens'=>$specimen,
					'formula'=>$formula,
			]);

			return redirect('master-formula');
		}

		public function delete($id) {
		    $query = DB::table('master_formula')->where('id', $id);
		    $data_test = $query->first();
		    DB::delete('delete from master_formula where id = ?',[$id]);
		    
		    return redirect('master-formula');
		}
}
