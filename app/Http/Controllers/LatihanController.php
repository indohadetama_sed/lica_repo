<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LatihanController extends Controller
{
    public function index(){
    	$judul = "Home";
    	$nama = "Anshar";
    	$data['title'] = $judul;
    	$data['footer'] = $nama;
    	return view('latihan.index',$data);
    }

    //DATABASE
    public function table(){
        $judul = "Table";
        $nama = "Anshar";

        $query = DB::table('tb_mhs')
            ->select('tb_mhs.id','tb_mhs.nama','tb_mhs.jumlah','tb_tipe.tipe')
            ->leftJoin('tb_tipe','tb_mhs.tipe','=','tb_tipe.id');
        $data_mhs = $query->get();

    	$data['title'] = $judul;
    	$data['footer'] = $nama;
        $data['mahasiswas'] = $data_mhs;
    	return view('latihan.table',$data);
    }
    
    //GET
    public function param($param){
    	$isi = $param;
    	$judul = "Param";
    	$nama = "Anshar";

    	$data['title'] = $judul;
    	$data['footer'] = $nama;
    	$data['param'] = $isi;
    	return view('latihan.param',$data);
    }

    public function param2($param, $param2){
    	
    	$judul = "Param";
    	$nama = "Anshar";
    	$data['title'] = $judul;
    	$data['footer'] = $nama;
    	$data['a'] = $param;
    	$data['b'] = $param2;
    	$data['c'] = $param + $param2;
    	return view('latihan.tambahGet',$data);
    }

    public function input(){
    	$judul = "Input";
    	$nama = "Anshar";

        $query = DB::table('tb_tipe');
        $data_tipe = $query->get();

    	$data['title'] = $judul;
    	$data['footer'] = $nama;
        $data['tipes'] = $data_tipe;
    	return view('latihan.input',$data);
    }

    //POST
    public function inputPost(Request $request){   	
    	$judul = "Hasil Input";
    	$nama = "Anshar";

        $nama = $request->input('nama');
        $jumlah = $request->input('jumlah');
        $tipe = $request->input('tipe');

        DB::table('tb_mhs')
        ->insert(
            ['nama'=>$nama,
            'jumlah'=>$jumlah,
            'tipe'=>$tipe
            ]);

    	$data['title'] = $judul;
    	$data['footer'] = $nama;
    	return redirect('table');
    }

    public function inputTambah(){
    	$judul = "inputPost";
    	$nama = "Anshar";
    	$data['title'] = $judul;
    	$data['footer'] = $nama;
    	return view('latihan.inputTambah',$data);
    }

    public function inputTambahPost(Request $request){
    	$judul = "inputPost";
    	$nama = "Anshar";
    	$data['title'] = $judul;
    	$data['footer'] = $nama;

    	$angka1 = $request->input('angka1');
    	$angka2 = $request->input('angka2');
    	$data['angka1'] = $angka1;
    	$data['angka2'] = $angka2;
    	$data['hasil'] = $angka1 + $angka2;
    	return view('latihan.hasilTambahPost',$data);
    }

    //GET DETAIL DATA
    public function detail($id_mhs){
        $judul = "View Detail";
        $nama = "Anshar";

        $query = DB::table('tb_mhs')->where('id', $id_mhs);
        $data_mhs = $query->first();

        $data['title'] = $judul;
        $data['footer'] = $nama;
        $data['detail'] = $data_mhs;
        return view('latihan.viewDetail',$data);
    }

    //UPDATE
    public function update($id_mhs){
        $judul = "Update";
        $nama = "Anshar";

        $query = DB::table('tb_mhs')->where('id', $id_mhs);
        $data_mhs = $query->first();

        $data['title'] = $judul;
        $data['footer'] = $nama;
        $data['detail'] = $data_mhs;
        return view('latihan.update',$data);
    }

    public function updatePost(Request $request){    
        $judul = "Hasil Update";
        $nama = "Anshar";

        $id = $request->input('id');
        $nama = $request->input('nama');
        $jumlah = $request->input('jumlah');

        DB::table('tb_mhs')
        ->where('id', $id)
        ->update(
            ['nama' => $nama,
            'jumlah' => $jumlah
            ]);

        $data['title'] = $judul;
        $data['footer'] = $nama;
        return redirect('table');
    }

}
