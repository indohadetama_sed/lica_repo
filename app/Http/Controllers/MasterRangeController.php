<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MasterRangeController extends Controller
{
    public function index(){
    	$judul = "Master Range";

        $query = DB::table('master_ranges');
        $data_range = $query->get();

    	$data['title'] = $judul;
        $data['ranges'] = $data_range;

    	return view('pages.masters.master_range',$data);
    }

    public function input(Request $request){
        $age_min = $request->input('age-below');
        $age_max = $request->input('age-above');
        $male_min = $request->input('male-ref-below');
        $male_max = $request->input('male-ref-above');
        $male_crit_min = $request->input('male-crit-below');
        $male_crit_max = $request->input('male-crit-above');
        $female_min = $request->input('female-ref-below');
        $female_max = $request->input('female-ref-above');
        $female_crit_min = $request->input('female-crit-below');
        $female_crit_max = $request->input('female-crit-above');

        DB::table('master_ranges')
        ->insert(
            ['min_age'=>$age_min,
            'max_age'=>$age_max,
            'min_male'=>$male_min,
            'max_male'=>$male_max,
            'min_crit_male'=>$male_crit_min,
            'max_crit_male' =>$male_crit_max,
            'min_female'=>$female_min,
            'max_female'=>$female_max,
            'min_crit_female'=>$female_crit_min,
            'max_crit_female' =>$female_crit_max,
        ]);

    	return redirect('master-range');
    }

    //UPDATE
    public function update($id){
        $judul = "Edit Master Range";

        $query = DB::table('master_ranges')->where('id', $id);
        $data_test = $query->first();

        $data['title'] = $judul;
        $data['ranges'] = $data_test;
        return view('pages.masters.master_range_update',$data);
    }

    public function updatePost(Request $request){
        $id = $request->input('id');    
        $age_min = $request->input('age-below');
        $age_max = $request->input('age-above');
        $male_min = $request->input('male-ref-below');
        $male_max = $request->input('male-ref-above');
        $male_crit_min = $request->input('male-crit-below');
        $male_crit_max = $request->input('male-crit-above');
        $female_min = $request->input('female-ref-below');
        $female_max = $request->input('female-ref-above');
        $female_crit_min = $request->input('female-crit-below');
        $female_crit_max = $request->input('female-crit-above');

        DB::table('master_ranges')
        ->where('id', $id)
        ->update(
            ['min_age'=>$age_min,
            'max_age'=>$age_max,
            'min_male'=>$male_min,
            'max_male'=>$male_max,
            'min_crit_male'=>$male_crit_min,
            'max_crit_male' =>$male_crit_max,
            'min_female'=>$female_min,
            'max_female'=>$female_max,
            'min_crit_female'=>$female_crit_min,
            'max_crit_female' =>$female_crit_max,
        ]);

        return redirect('master-range');
    }

    public function delete($id) {
        $query = DB::table('master_ranges')->where('id', $id);
        $data_test = $query->first();
        DB::delete('delete from master_ranges where id = ?',[$id]);
        
        return redirect('master-range');
    }

}
