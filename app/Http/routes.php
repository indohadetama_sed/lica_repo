<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/anshar', function () {
    return view('welcome2');
});

Route::get('/master-test', 'MasterTestController@index');
Route::post('/master-test/input', 'MasterTestController@input');
Route::get('/master-test/update/{id}', 'MasterTestController@update');
Route::post('/master-test/update', 'MasterTestController@updatePost');
Route::get('/master-test/delete/{id}', 'MasterTestController@delete');

Route::get('/master-group', 'MasterGroupController@index');
Route::post('/master-group/input', 'MasterGroupController@input');
Route::get('/master-group/update/{id}', 'MasterGroupController@update');
Route::post('/master-group/update', 'MasterGroupController@updatePost');
Route::get('/master-group/delete/{id}', 'MasterGroupController@delete');

Route::get('/master-group-test', 'MasterGroupTestController@index');
Route::post('/master-group-test/input', 'MasterGroupTestController@input');
Route::get('/master-group-test/update/{id}', 'MasterGroupTestController@update');
Route::post('/master-group-test/update', 'MasterGroupTestController@updatePost');
Route::get('/master-group-test/delete/{id}', 'MasterGroupTestController@delete');

Route::get('/master-range', 'MasterRangeController@index');
Route::post('/master-range/input', 'MasterRangeController@input');
Route::get('/master-range/update/{id}', 'MasterRangeController@update');
Route::post('/master-range/update', 'MasterRangeController@updatePost');
Route::get('/master-range/delete/{id}', 'MasterRangeController@delete');

Route::get('/master-result', 'MasterResultController@index');
Route::post('/master-result/input', 'MasterResultController@input');
Route::get('/master-result/update/{id}', 'MasterResultController@update');
Route::post('/master-result/update', 'MasterResultController@updatePost');
Route::get('/master-result/delete/{id}', 'MasterResultController@delete');

Route::get('/master-result-range', 'MasterResultRangeController@index');
Route::post('/master-result-range/input', 'MasterResultRangeController@input');
Route::get('/master-result-range/update/{id}', 'MasterResultRangeController@update');
Route::post('/master-result-range/update', 'MasterResultRangeController@updatePost');
Route::get('/master-result-range/delete/{id}', 'MasterResultRangeController@delete');

Route::get('/master-analyzers', 'MasterAnalyzerController@index');
Route::post('/input-analyzer', 'MasterAnalyzerController@input');
Route::get('/update-analyzer/{id}', 'MasterAnalyzerController@update');
Route::post('/update-analyzer', 'MasterAnalyzerController@updatePost');
Route::get('/delete-analyzer/{id}', 'MasterAnalyzerController@delete');

Route::get('/master-specimens', 'MasterSpecimenController@index');
Route::post('/input-specimens', 'MasterSpecimenController@input');
Route::get('/update-specimens/{id}', 'MasterSpecimenController@update');
Route::post('/update-specimens', 'MasterSpecimenController@updatePost');
Route::get('/delete-specimens/{id}', 'MasterSpecimenController@delete');

Route::get('/master-prices', 'MasterPriceController@index');
Route::post('/input-prices', 'MasterPriceController@input');
Route::get('/update-prices/{id}', 'MasterPriceController@update');
Route::post('/update-prices', 'MasterPriceController@updatePost');
Route::get('/delete-prices/{id}', 'MasterPriceController@delete');

//belum beres
Route::get('/master-patient', 'MasterPatientController@index');
Route::post('/master-patient/input', 'MasterPatientController@input');
Route::get('/master-patient/update/{id}', 'MasterPatientController@update');
Route::post('/update-patient', 'MasterPatientController@updatePost');
Route::get('/master-patient/delete/{id}', 'MasterPatientController@delete');

Route::get('/master-room', 'MasterRoomController@index');
Route::post('/master-room/input', 'MasterRoomController@input');
Route::get('/master-room/update/{id}', 'MasterRoomController@update');
Route::post('/update-room', 'MasterRoomController@updatePost');
Route::get('/master-room/delete/{id}', 'MasterRoomController@delete');

Route::get('/master-insurace', 'MasterInsuraceController@index');
Route::post('/master-insurace/input', 'MasterInsuraceController@input');
Route::get('/master-insurace/update/{id}', 'MasterInsuraceController@update');
Route::post('/update-insurace', 'MasterInsuraceController@updatePost');
Route::get('/master-insurace/delete/{id}', 'MasterInsuraceController@delete');

Route::get('/master-dokter', 'MasterDokterController@index');
Route::post('/master-dokter/input', 'MasterDokterController@input');
Route::get('/master-dokter/update/{id}', 'MasterDokterController@update');
Route::post('/update-dokter', 'MasterDokterController@updatePost');
Route::get('/master-dokter/delete/{id}', 'MasterDokterController@delete');

Route::get('/master-formula', 'MasterFormulaController@index');
Route::post('/master-formula/input', 'MasterFormulaController@input');
Route::get('/master-formula/update/{id}', 'MasterFormulaController@update');
Route::post('/update-formula', 'MasterFormulaController@updatePost');
Route::get('/master-formula/delete/{id}', 'MasterFormulaController@delete');