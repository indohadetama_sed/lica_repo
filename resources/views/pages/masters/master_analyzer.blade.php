@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('/input-analyzer')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Name</label>
			<input type="text" name="name"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>No</td>
				<td>Names</td>
			</tr>
			@foreach($analyzers as $index => $master_analyzers)
			<tr>
				<td>{{$index +1}}</td>
				<td>{{$master_analyzers->analyzer}}</td>
				<td>
					<a href="{{ URL::to('/update-analyzer/' . $master_analyzers->id) }}">Update</a>
				||
					<a href="{{ URL::to('/delete-analyzer/' . $master_analyzers->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

