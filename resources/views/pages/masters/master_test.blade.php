@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master_test/input')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Name</label>
			<input type="text" name="name"><br><br>

			<label>Initial</label>
			<input type="text" name="initial" style="text-transform:uppercase"><br><br>

			<label>Unit</label>
			<input type="number" name="unit"><br><br>

			<label>Analyzer</label>
			<input type="text" name="analyzer"><br><br>

			<label>Specimen</label>
			<input type="text" name="specimen"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>ID</td>
				<td>Names</td>
				<td>Initial</td>
				<td>Unit</td>
				<td>Master Analyzer ID</td>
				<td>Master specimen ID</td>
				<td></td>
			</tr>
			@foreach($tests as $test)
			<tr>
				<td>{{$test->id}}</td>
				<td>{{$test->name}}</td>
				<td>{{$test->initial}}</td>
				<td>{{$test->unit}}</td>
				<td>{{$test->master_analyzer_id}}</td>
				<td>{{$test->master_specimen_id}}</td>
				<td>
					<a href="{{ URL::to('master-test/update/' . $test->id) }}">Update</a>
				||
					<a href="{{ URL::to('master-test/delete/' . $test->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

