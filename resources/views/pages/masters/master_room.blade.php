@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master-room/input')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Ruangan</label>
			<input type="text" name="ruangan"><br><br>

			<label>Kelas</label>
			<input type="text" name="kelas"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>No</td>
				<td>Ruangan</td>
				<td>Kelas</td>
				<td>Check In</td>
				<td>Auto Draw</td>
			</tr>
			@foreach($room as $index => $master_room)
			<tr>
				<td>{{$index +1}}</td>
				<td>{{$master_room->room}}</td>
				<td>{{$master_room->kelas}}</td>
				<td>{{$master_room->auto_checkin}}</td>
				<td>{{$master_room->auto_draw}}</td>
				<td>
					<a href="{{ URL::to('master-room/update/' . $master_room->id) }}">Update</a>
				||
					<a href="{{ URL::to('master-room/delete/' . $master_room->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

