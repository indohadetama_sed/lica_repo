@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master_test/update')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$tests->id}}">

			<label>ID</label>
			<input type="text" name="" value="{{$tests->id}}" disabled><br><br>

			<label>Name</label>
			<input type="text" name="name" value="{{$tests->name}}"><br><br>

			<label>Initial</label>
			<input type="text" name="initial" style="text-transform:uppercase" value="{{$tests->initial}}"><br><br>

			<label>Unit</label>
			<input type="number" name="unit" value="{{$tests->unit}}"><br><br>

			<label>Analyzer</label>
			<input type="text" name="analyzer" value="{{$tests->master_analyzer_id}}"><br><br>

			<label>Specimen</label>
			<input type="text" name="specimen" value="{{$tests->master_specimen_id}}"><br><br>

			<input type="submit" value="Update">
		</form>
	</div>
@stop

