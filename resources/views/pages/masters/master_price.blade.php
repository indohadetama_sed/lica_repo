@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('/input-prices')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Name</label>
			<input type="text" name="name"><br><br>

			<label>Master Test</label>
			    <select name="master_tes">
					@foreach ($master_tes as $master_tests)
			            <option value="{{ $master_tests->id }}">
			                {{ $master_tests->name }}           
			            </option>
			        @endforeach
				</select>
			</br>
			</br>
			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>No</td>
				<td>Names</td>
				<td>Master Test</td>
			</tr>
			@foreach($prices as $index => $master_prices)
			<tr>
				<td>{{$index +1}}</td>
				<td>{{$master_prices->price}}</td>
				<td>{{$master_prices->name}}</td>
				<td>
					<a href="{{ URL::to('/update-prices/' . $master_prices->id) }}">Update</a>
				||
					<a href="{{ URL::to('/delete-prices/' . $master_prices->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

