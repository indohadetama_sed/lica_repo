@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master_group/input')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Name</label>
			<input type="text" name="name"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>ID</td>
				<td>Names</td>
				<td></td>
			</tr>
			@foreach($groups as $group)
			<tr>
				<td>{{$group->id}}</td>
				<td>{{$group->name}}</td>
				<td>
					<a href="{{ URL::to('master-group/update/' . $group->id) }}">Update</a>
				||
					<a href="{{ URL::to('master-group/delete/' . $group->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

