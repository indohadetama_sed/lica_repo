@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master-result-range/update')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$result_ranges->id}}">

			<label>ID</label>
			<input type="text" name="" value="{{$result_ranges->id}}" disabled><br><br>

			<label>Age Below</label>
			<input type="number" name="age-below" value="{{$result_ranges->min_age}}"><br><br>

			<label>Age Above</label>
			<input type="number" name="age-above" value="{{$result_ranges->max_age}}"><br><br>

			<label>Male Ref</label>
			<input type="text" name="male-ref" value="{{$result_ranges->male_ref}}"><br><br>

			<label>Female Ref</label>
			<input type="text" name="female-ref" value="{{$result_ranges->female_ref}}"><br><br>

			<label>Male Crit</label>
			<input type="text" name="male-crit" value="{{$result_ranges->male_crit}}"><br><br>

			<label>Female Crit</label>
			<input type="text" name="female-crit" value="{{$result_ranges->female_crit}}"><br><br>

			<input type="submit" value="Update">
		</form>
	</div>
@stop

