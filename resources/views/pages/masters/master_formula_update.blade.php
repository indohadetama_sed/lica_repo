@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('/update-formula')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$formula->id}}">

			<label>Id Test</label>
			<input type="text" name="id_test" value="{{$formula->id_test}}"><br><br>

			<label>Master Prices</label>
				<select name="prices">
				  @foreach ($price as $master_prices)
				    <option 
				      @if($formula-> prices == $master_prices->id)
				        Selected
				      @endif
				      value="{{ $master_prices->id }}">
				      {{ $master_prices->price }}       
				    </option>
				  @endforeach
				</select><br><br>

			<label>Master Analyzers</label>
				<select name="analyzer">
				  @foreach ($analyzer as $master_analyzers)
				    <option 
				      @if($formula-> analyzer == $master_analyzers->id)
				        Selected
				      @endif
				      value="{{ $master_analyzers->id }}">
				      {{ $master_analyzers->analyzer }}       
				    </option>
				  @endforeach
				</select><br><br>

			<label>Master Specimens</label>
				<select name="specimen">
				  @foreach ($specimen as $master_specimens)
				    <option 
				      @if($formula-> specimens == $master_specimens->id)
				        Selected
				      @endif
				      value="{{ $master_specimens->id }}">
				      {{ $master_specimens->specimens }}       
				    </option>
				  @endforeach
				</select><br><br>

			<label>Formula</label>
			<input type="text" name="formula" value="{{$formula->formula}}"><br><br>

			<input type="submit" value="Update">
		</form>
	</div>
@stop

