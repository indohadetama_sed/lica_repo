@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('/update-dokter')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$dokter->id}}">

			<label>Nama</label>
			<input type="text" name="nama" value="{{$dokter->name}}"><br><br>

			<label>Title Dokter</label>
			<input type="text" name="title_dokter" value="{{$dokter->title_doctor}}"><br><br>

			<input type="submit" value="Update">
		</form>
	</div>
@stop

