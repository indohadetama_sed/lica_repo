@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master_group/update')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$groups->id}}">

			<label>ID</label>
			<input type="text" name="" value="{{$groups->id}}" disabled><br><br>

			<label>Name</label>
			<input type="text" name="name" value="{{$groups->name}}"><br><br>


			<input type="submit" value="Update">
		</form>
	</div>
@stop

