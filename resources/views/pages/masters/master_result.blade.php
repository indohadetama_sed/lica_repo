@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master-result/input')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Result</label>
			<input type="text" name="result"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>ID</td>
				<td>Master Test ID</td>
				<td>Results</td>
			</tr>
			@foreach($results as $result)
			<tr>
				<td>{{$result->id}}</td>
				<td>{{$result->master_test_id}}</td>
				<td>{{$result->result}}</td>
				<td>
					<a href="{{ URL::to('master-result/update/' . $result->id) }}">Update</a>
				||
					<a href="{{ URL::to('master-result/delete/' . $result->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

