@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('/update-specimens')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$specimens->id}}">

			<label>Name</label>
			<input type="text" name="nama" value="{{$specimens->specimens}}"><br><br>

			<input type="submit" value="Update">
		</form>
	</div>
@stop

