@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('/input-specimens')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Name</label>
			<input type="text" name="name"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>No</td>
				<td>Names</td>
			</tr>
			@foreach($specimens as $index => $master_specimens)
			<tr>
				<td>{{$index +1}}</td>
				<td>{{$master_specimens->specimens}}</td>
				<td>
					<a href="{{ URL::to('/update-specimens/' . $master_specimens->id) }}">Update</a>
				||
					<a href="{{ URL::to('/delete-specimens/' . $master_specimens->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

