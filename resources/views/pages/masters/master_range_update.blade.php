@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master-range/update')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$ranges->id}}">

			<label>ID</label>
			<input type="text" name="" value="{{$ranges->id}}" disabled><br><br>

			<label>Age Below</label>
			<input type="number" name="age-below" value="{{$ranges->min_age}}"><br><br>

			<label>Age Above</label>
			<input type="number" name="age-above" value="{{$ranges->max_age}}"><br><br>

			<label>Male Ref Below</label>
			<input type="text" name="male-ref-below" value="{{$ranges->min_male}}"><br><br>

			<label>Male Ref Above</label>
			<input type="text" name="male-ref-above" value="{{$ranges->max_male}}"><br><br>

			<label>Male Crit Below</label>
			<input type="text" name="male-crit-below" value="{{$ranges->min_crit_male}}"><br><br>

			<label>Male Crit Above</label>
			<input type="text" name="male-crit-above" value="{{$ranges->max_crit_male}}"><br><br>

			<label>Female Ref Below</label>
			<input type="text" name="female-ref-below" value="{{$ranges->min_female}}"><br><br>

			<label>Female Ref Above</label>
			<input type="text" name="female-ref-above" value="{{$ranges->max_female}}"><br><br>

			<label>Female Crit Below</label>
			<input type="text" name="female-crit-below" value="{{$ranges->min_crit_female}}"><br><br>

			<label>Female Crit Above</label>
			<input type="text" name="female-crit-above" value="{{$ranges->max_crit_female}}"><br><br>

			<input type="submit" value="Update">
		</form>
	</div>
@stop

