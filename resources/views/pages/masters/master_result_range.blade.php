@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master-result-range/input')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Age Below</label>
			<input type="number" name="age-below"><br><br>

			<label>Age Above</label>
			<input type="number" name="age-above"><br><br>

			<label>Male Ref</label>
			<input type="text" name="male-ref"><br><br>

			<label>Female Ref</label>
			<input type="text" name="female-ref"><br><br>

			<label>Male Crit</label>
			<input type="text" name="male-crit"><br><br>

			<label>Female Crit</label>
			<input type="text" name="female-crit"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>ID</td>
				<td>Master Test ID</td>
				<td>Age Below</td>
				<td>Age Above</td>
				<td>Male Ref</td>
				<td>Female Ref</td>
				<td>Male Crit</td>
				<td>Female Crit</td>
				<td></td>
			</tr>
			@foreach($result_ranges as $result_range)
			<tr>
				<td>{{$result_range->id}}</td>
				<td>{{$result_range->master_test_id}}</td>
				<td>{{$result_range->min_age}}</td>
				<td>{{$result_range->max_age}}</td>
				<td>{{$result_range->male_ref}}</td>
				<td>{{$result_range->female_ref}}</td>
				<td>{{$result_range->male_crit}}</td>
				<td>{{$result_range->female_crit}}</td>
				<td>
					<a href="{{ URL::to('master-result-range/update/' . $result_range->id) }}">Update</a>
				||
					<a href="{{ URL::to('master-result-range/delete/' . $result_range->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

