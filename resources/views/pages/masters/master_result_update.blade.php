@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master-result/update')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$results->id}}">

			<label>ID</label>
			<input type="text" name="" value="{{$results->id}}" disabled><br><br>

			<label>Result</label>
			<input type="text" name="result" value="{{$results->result}}"><br><br>


			<input type="submit" value="Update">
		</form>
	</div>
@stop
