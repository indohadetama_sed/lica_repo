@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('/update-analyzer')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$analyzer->id}}">

			<label>Name</label>
			<input type="text" name="nama" value="{{$analyzer->analyzer}}"><br><br>

			<input type="submit" value="Update">
		</form>
	</div>
@stop

