@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master-range/input')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Age Below</label>
			<input type="number" name="age-below"><br><br>

			<label>Age Above</label>
			<input type="number" name="age-above"><br><br>

			<label>Male Ref Below</label>
			<input type="text" name="male-ref-below"><br><br>

			<label>Male Ref Above</label>
			<input type="text" name="male-ref-above"><br><br>

			<label>Male Crit Below</label>
			<input type="text" name="male-crit-below"><br><br>

			<label>Male Crit Above</label>
			<input type="text" name="male-crit-above"><br><br>

			<label>Female Ref Below</label>
			<input type="text" name="female-ref-below"><br><br>

			<label>Female Ref Above</label>
			<input type="text" name="female-ref-above"><br><br>

			<label>Female Crit Below</label>
			<input type="text" name="female-crit-below"><br><br>

			<label>Female Crit Above</label>
			<input type="text" name="female-crit-above"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>ID</td>
				<td>Master Test ID</td>
				<td>Age Below</td>
				<td>Age Above</td>
				<td>Male Ref Below</td>
				<td>Male Ref Above</td>
				<td>Male Crit Below</td>
				<td>Male Crit Above</td>
				<td>Female Ref Below</td>
				<td>Female Ref Above</td>
				<td>Female Crit Below</td>
				<td>Female Crit Below</td>
				<td></td>
			</tr>
			@foreach($ranges as $range)
			<tr>
				<td>{{$range->id}}</td>
				<td>{{$range->master_test_id}}</td>
				<td>{{$range->min_age}}</td>
				<td>{{$range->max_age}}</td>
				<td>{{$range->min_male}}</td>
				<td>{{$range->max_male}}</td>
				<td>{{$range->min_crit_male}}</td>
				<td>{{$range->max_crit_male}}</td>
				<td>{{$range->min_female}}</td>
				<td>{{$range->max_female}}</td>
				<td>{{$range->min_crit_female}}</td>
				<td>{{$range->max_crit_female}}</td>
				
				<td>
					<a href="{{ URL::to('master-range/update/' . $range->id) }}">Update</a>
				||
					<a href="{{ URL::to('master-range/delete/' . $range->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

