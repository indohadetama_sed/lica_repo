@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master_group_test/input')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Name</label>
			<select name="group">
				@foreach($groups as $group)
				<option value="{{$group->id}}">{{$group->name}}</option>
				@endforeach
			</select><br><br>

			<label>Test</label><br>
			@foreach($tests as $test)
			<input type="checkbox" name="tests[]" value="{{$test->id}}"> {{$test->name}}<br>
			@endforeach

			<br>
			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>ID</td>
				<td>Name</td>
				<td>Test</td>
				<td></td>
			</tr>
			@foreach($group_tests as $group_test)
			<tr>
				<td>{{$group_test->id}}</td>
				<td>{{$group_test->master_group_id}}</td>
				<td>{{$group_test->master_test_id}}</td>
				<td>
					<a href="{{ URL::to('master-group-test/update/' . $group_test->id) }}">Update</a>
				||
					<a href="{{ URL::to('master-group-test/delete/' . $group_test->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

