@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('/update-room')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$room->id}}">

			<label>Ruangan</label>
			<input type="text" name="ruangan" value="{{$room->room}}"><br><br>

			<label>Kelas</label>
			<input type="text" name="kelas" value="{{$room->kelas}}"><br><br>

			<input type="submit" value="Update">
		</form>
	</div>
@stop

