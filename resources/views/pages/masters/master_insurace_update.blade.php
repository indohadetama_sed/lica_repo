@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('/update-insurace')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$insurace->id}}">

			<label>Insurace</label>
			<input type="text" name="insurace" value="{{$insurace->nama_insurace}}"><br><br>

			<label>Discount</label>
			<input type="text" name="discount" value="{{$insurace->discount}}"><br><br>

			<input type="submit" value="Update">
		</form>
	</div>
@stop

