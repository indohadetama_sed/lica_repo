@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('/update-prices')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$master_pri->id}}">

			<label>Name</label>
			<input type="text" name="nama" value="{{$master_pri->price}}"><br><br>

			<label>Master Test</label>
				<select name="master_test">
				  @foreach ($master_tes as $master_tests)
				    <option 
				      @if($master_pri-> master_test_id == $master_tests->id)
				        Selected
				      @endif
				      value="{{ $master_tests->id }}">
				      {{ $master_tests->name }}       
				    </option>
				  @endforeach
				</select>
			<br><br>
			<input type="submit" value="Update">
		</form>
	</div>
@stop

