@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master-dokter/input')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Nama</label>
			<input type="text" name="name"><br><br>

			<label>Title Dokter</label>
			<input type="text" name="title_doctor"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>No</td>
				<td>Nama</td>
				<td>Title Dokter</td>
			</tr>
			@foreach($dokter as $index => $master_dokter)
			<tr>
				<td>{{$index +1}}</td>
				<td>{{$master_dokter->name}}</td>
				<td>{{$master_dokter->title_doctor}}</td>
				<td>
					<a href="{{ URL::to('master-dokter/update/' . $master_dokter->id) }}">Update</a>
				||
					<a href="{{ URL::to('master-dokter/delete/' . $master_dokter->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

