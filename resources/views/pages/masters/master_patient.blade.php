@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master-patient/input')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Name</label>
			<input type="text" name="nama"><br><br>

			<label>Ruangan</label>
			    <select name="ruangan">
					@foreach ($room as $master_room)
			            <option value="{{ $master_room->id }}">
			                {{ $master_room->room }} 
			            </option>
			        @endforeach
				</select><br><br>

			<label>Jenis Kelamin</label>
			<input type="text" name="jenis_kelamin"><br><br>

			<label>Umur</label>
			<input type="text" name="umur"><br><br>

			<label>Alamat</label>
			<input type="text" name="alamat"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>No</td>
				<td>Nama</td>
				<td>Ruangan</td>
				<td>Jenis Kelamin</td>
				<td>Umur</td>
				<td>Alamat</td>
			</tr>
			@foreach($patient as $index => $master_patient)
			<tr>
				<td>{{$index +1}}</td>
				<td>{{$master_patient->nama}}</td>
				<td>{{$master_patient->room}}</td>
				<td>{{$master_patient->jenis_kelamin}}</td>
				<td>{{$master_patient->umur}}</td>
				<td>{{$master_patient->alamat}}</td>
				<td>
					<a href="{{ URL::to('master-patient/update/' . $master_patient->id) }}">Update</a>
				||
					<a href="{{ URL::to('master-patient/delete/' . $master_patient->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

