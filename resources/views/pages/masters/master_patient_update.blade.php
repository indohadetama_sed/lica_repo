@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('/update-patient')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$patient->id}}">

			<label>Name</label>
			<input type="text" name="nama" value="{{$patient->nama}}"><br><br>

			<label>Ruangan</label>
				<select name="ruangan">
				  @foreach ($room as $master_room)
				    <option 
				      @if($patient-> no_rm == $master_room->id)
				        Selected
				      @endif
				      value="{{ $master_room->id }}">
				      {{ $master_room->room }}       
				    </option>
				  @endforeach
				</select><br><br>

			<label>Jenis Kelamin</label>
			<input type="text" name="jenis_kelamin" value="{{$patient->jenis_kelamin}}"><br><br>

			<label>Umur</label>
			<input type="text" name="umur" value="{{$patient->umur}}"><br><br>

			<label>Alamat</label>
			<input type="text" name="alamat" value="{{$patient->alamat}}"><br><br>
			<input type="submit" value="Update">
		</form>
	</div>
@stop

