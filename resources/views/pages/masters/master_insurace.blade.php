@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master-insurace/input')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Insurace</label>
			<input type="text" name="insurace"><br><br>

			<label>Discount</label>
			<input type="text" name="discount"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>No</td>
				<td>Insurace</td>
				<td>Discount</td>
			</tr>
			@foreach($insurace as $index => $master_insurace)
			<tr>
				<td>{{$index +1}}</td>
				<td>{{$master_insurace->nama_insurace}}</td>
				<td>{{$master_insurace->discount}}</td>
				<td>
					<a href="{{ URL::to('master-insurace/update/' . $master_insurace->id) }}">Update</a>
				||
					<a href="{{ URL::to('master-insurace/delete/' . $master_insurace->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

