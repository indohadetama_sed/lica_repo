@extends('layout.backend')

@section('content')
	<div>
		<form action="{{url('master-formula/input')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<label>Id Test</label>
			<input type="text" name="id-test"><br><br>

			<label>Master Prices</label>
			    <select name="master-prices">
					@foreach ($price as $master_prices)
			            <option value="{{ $master_prices->id }}">
			                {{ $master_prices->price }}           
			            </option>
			        @endforeach
				</select><br><br>

			<label>Master Analyzers</label>
			    <select name="master-analyzer">
					@foreach ($analyzer as $master_analyzers)
			            <option value="{{ $master_analyzers->id }}">
			                {{ $master_analyzers->analyzer }}           
			            </option>
			        @endforeach
				</select><br><br>

			<label>Master Specimen</label>
			    <select name="master-specimen">
					@foreach ($specimen as $master_specimens)
			            <option value="{{ $master_specimens->id }}">
			                {{ $master_specimens->specimens }}           
			            </option>
			        @endforeach
				</select><br><br>

			<label>Formula</label>
			<input type="text" name="formula"><br><br>

			<input type="submit">
		</form>
	</div>

	<div>
		<br><br>
		<h3>{{$title}} Table</h3>
		<table border="1">
			<tr>
				<td>No</td>
				<td>Id Test</td>
				<td>Master Prices</td>
				<td>Master Analyzer</td>
				<td>Master Specimens</td>
				<td>Formula</td>
			</tr>
			@foreach($formula as $index => $master_formula)
			<tr>
				<td>{{$index +1}}</td>
				<td>{{$master_formula->id_test}}</td>
				<td>{{$master_formula->price}}</td>
				<td>{{$master_formula->analyzer}}</td>
				<td>{{$master_formula->specimens}}</td>
				<td>{{$master_formula->formula}}</td>
				<td>
					<a href="{{ URL::to('master-formula/update/' . $master_formula->id) }}">Update</a>
				||
					<a href="{{ URL::to('master-formula/delete/' . $master_formula->id) }}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

@stop

