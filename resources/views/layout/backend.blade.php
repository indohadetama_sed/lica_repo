<!DOCTYPE html>
<html>
<head>
	<title>{{$title}}</title>
	@include('include.head')

</head>
<body>

	@include('include.header')
	@yield('content')
	@include('include.footer')
</body>
</html>