@extends('layout.backend')
@section('content')

	<!-- Content-->
	<div>
		<table border="1">
			<tr>
				<td>No</td>
				<td>Nama</td>
				<td>Jumlah</td>
				<td>Tipe</td>
			</tr>
			@foreach($mahasiswas as $mahasiswa)
			<tr>
				<td>{{$mahasiswa->id}}</td>
				<td>{{$mahasiswa->nama}}</td>
				<td>{{$mahasiswa->jumlah}}</td>
				<td>{{$mahasiswa->tipe}}</td>
			</tr>
			@endforeach
		</table>
	</div>	
@stop