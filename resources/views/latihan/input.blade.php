@extends('layout.backend')
@section('content')


	<!-- Content-->
	<div>
		<form action="input" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<label>Nama</label>
				<input type="text" name="nama" >
				<br><br>

			<label>Jumlah</label>
				<input type="number" name="jumlah">
				<br><br>

			<label>Tipe</label>
				<select name="tipe">
					@foreach($tipes as $tipe)
					<option value="{{$tipe->id}}">{{$tipe->tipe}}</option>
					@endforeach
				</select>	

				<input type="submit">
		</form>	
	</div>	

@stop