/*
 Navicat Premium Data Transfer

 Source Server         : lica
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : db_lica

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 17/04/2019 22:38:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for master_group_tests
-- ----------------------------
DROP TABLE IF EXISTS `master_group_tests`;
CREATE TABLE `master_group_tests`  (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `master_test_id` int(9) NULL DEFAULT NULL,
  `master_group_id` int(9) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of master_group_tests
-- ----------------------------
INSERT INTO `master_group_tests` VALUES (1, 5, 3);
INSERT INTO `master_group_tests` VALUES (2, 6, 3);
INSERT INTO `master_group_tests` VALUES (3, 8, 3);
INSERT INTO `master_group_tests` VALUES (4, 10, 3);
INSERT INTO `master_group_tests` VALUES (5, 11, 3);

-- ----------------------------
-- Table structure for master_groups
-- ----------------------------
DROP TABLE IF EXISTS `master_groups`;
CREATE TABLE `master_groups`  (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of master_groups
-- ----------------------------
INSERT INTO `master_groups` VALUES (3, 'Demam Berdarah');
INSERT INTO `master_groups` VALUES (4, 'Prostat');
INSERT INTO `master_groups` VALUES (5, 'TBC');
INSERT INTO `master_groups` VALUES (6, 'A');

-- ----------------------------
-- Table structure for master_ranges
-- ----------------------------
DROP TABLE IF EXISTS `master_ranges`;
CREATE TABLE `master_ranges`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `master_test_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `min_age` int(255) NULL DEFAULT NULL,
  `max_age` int(255) NULL DEFAULT NULL,
  `min_male` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `max_male` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `min_female` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `max_female` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `min_crit_male` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `max_crit_male` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `min_crit_female` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `max_crit_female` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of master_ranges
-- ----------------------------
INSERT INTO `master_ranges` VALUES (1, '', 0, 99, '1', '59', '1', '66', '15', '30', '17', '20');
INSERT INTO `master_ranges` VALUES (5, NULL, 2, 4, '0.5', '60', '0.5', '55', '30', '50', '25', '40');

-- ----------------------------
-- Table structure for master_result_ranges
-- ----------------------------
DROP TABLE IF EXISTS `master_result_ranges`;
CREATE TABLE `master_result_ranges`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `master_test_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `min_age` int(255) NULL DEFAULT NULL,
  `max_age` int(255) NULL DEFAULT NULL,
  `male_ref` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `female_ref` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `male_crit` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `female_crit` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of master_result_ranges
-- ----------------------------
INSERT INTO `master_result_ranges` VALUES (1, NULL, 1, 99, '1.5', '2.2', '5', '7');

-- ----------------------------
-- Table structure for master_results
-- ----------------------------
DROP TABLE IF EXISTS `master_results`;
CREATE TABLE `master_results`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `master_test_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `result` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of master_results
-- ----------------------------
INSERT INTO `master_results` VALUES (1, NULL, 'kuning');
INSERT INTO `master_results` VALUES (2, NULL, 'coklat');
INSERT INTO `master_results` VALUES (8, NULL, 'Merah');
INSERT INTO `master_results` VALUES (9, NULL, 'Ungu');

-- ----------------------------
-- Table structure for master_tests
-- ----------------------------
DROP TABLE IF EXISTS `master_tests`;
CREATE TABLE `master_tests`  (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `initial` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `unit` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `master_analyzer_id` int(9) NULL DEFAULT NULL,
  `master_specimen_id` int(9) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of master_tests
-- ----------------------------
INSERT INTO `master_tests` VALUES (5, 'Pipis', 'PPS', '148', 0, 0);
INSERT INTO `master_tests` VALUES (6, 'Tulang', 'ca', '13', 0, 0);
INSERT INTO `master_tests` VALUES (8, 'Feses', 'EE', '88', 0, 0);
INSERT INTO `master_tests` VALUES (10, 'Darah', 'HB', '14', 0, 0);
INSERT INTO `master_tests` VALUES (11, 'Rambut', 'DNA', '22', 0, 0);
INSERT INTO `master_tests` VALUES (13, 'LOL', 'YeAH', '1212', 122, 0);
INSERT INTO `master_tests` VALUES (14, 'Ase12', 'AS', '22', 122, 0);

SET FOREIGN_KEY_CHECKS = 1;
